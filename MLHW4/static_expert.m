function [ output_args ] = static_expert( d, b, data )
%Static_Expert Simple online learning algorithm
%   Joseph Haaga Machine Learning
t=1;

p_t= ones(size(data,1),d).*(1/d);

size(p_t)

prediction = zeros(size(data,1),1);

previousloss = 0;

% use 7th column as labels
labels = data(:,7);
% remove 7th column from input
data(:,7) = [];

for j=1:size(data,1);

    
    % my code
    prediction(j) = sum(p_t(j,:).*data(j,:));

    ttlsum =0;
    for i=1:d
        loss(j,i) = (data(j,i)-labels(j))^2;
        p_t(j+1,i) = p_t(j,i)*exp(-b*loss(j,i));
        ttlsum=ttlsum+p_t(j+1,i);   
    end
    mean = ttlsum/d;
    stddev = std(p_t(j+1,:));
    for r=1:d
        p_t(j+1,r) = ( p_t(j+1,r) - mean ) / stddev;
    end
end

%compute loss
L = zeros(size(loss, 1), 1);
for t = 1:size(loss, 1)
    L(t) = sum(loss(t, :))/t;
end
 plot(L);

output_args = struct('loss',loss,'p_t',p_t);

end

