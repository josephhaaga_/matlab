function [ output_args ] = testLasso( B, testX, testY )
%TestLasso testLasso - Mean Squared Test Error
%   Detailed explanation goes here

% Evaluate test error for each Lambda
for i=1:size(B,2)
   mse = 0;
   w = B(:,i)'; 
   % Calculate mean squared error
   for j=1:size(testX,1)
       x = testX(j,:)';
       %x=x-mean(x)/std(x);l
       % mse = mse + (((w*x)*std(testY)+mean(testY))-testY(j))^2;
       mse = mse + (((w*x))-testY(j))^2;
   end
   mse = mse/size(testX,1);
   M(i)= mse;
end
output_args = M;
end
