% Joseph Haaga 4.A

function [ result ] = compute_set_of_means( xvalues, labels )
%Compute_Set_Of_Means Returns average bitmaps for each numerical char
%   Bitmap images and relevant labels are averaged and each classes' result is returned.
    figure;
    colormap(1.0-gray);
    X = size(xvalues,1);
    uno=[];
    dos=[];
    tres=[];
    quattro=[];
    cinco=[];
    seis=[];
    siete=[];
    ocho=[];
    nueve=[];
    cero=[];
    % For each data point
   
    for m=1:X
%        X(m)=image(reshape(xvalues(m,:), [28 28])');
       % sort by class
       if labels(m)==1
           uno(end+1)=m;
           
       elseif labels(m)==2
           dos(end+1)=m;
           
       elseif labels(m)==3
           tres(end+1)=m;
           
       elseif labels(m)==4
           quattro(end+1)=m;
           
       elseif labels(m)==5
           cinco(end+1)=m;
           
       elseif labels(m)==6
           seis(end+1)=m;
           
       elseif labels(m)==7
           siete(end+1)=m;
           
       elseif labels(m)==8
           ocho(end+1)=m;
           
       elseif labels(m)==9
           nueve(end+1)=m;
           
       elseif labels(m)==0
           cero(end+1)=m;
           
       end
    end
    
     meanOne = zeros(28);
     
     for i=1:size(uno,2)
         new=reshape(xvalues(uno(i),:), [28 28])';
         meanOne = meanOne + new;
         image(meanOne);
%          % pause(0.1);
     end
     
     meanOne = meanOne ./ size(uno,2);
     
     image(meanOne);
     % pause(0.5);
     
     
     
     meanTwo = zeros(28);
     
     for i=1:size(dos,2)
         new=reshape(xvalues(dos(i),:), [28 28])';
         meanTwo = meanTwo + new;
         image(meanTwo);
         % pause(0.1);
     end
     
     meanTwo = meanTwo ./ size(dos,2);
     
     image(meanTwo);
     % pause(0.5);
     
     meanThree = zeros(28);
     
     for i=1:size(tres,2)
         new=reshape(xvalues(tres(i),:), [28 28])';
         meanThree = meanThree + new;
         image(meanThree);
         % pause(0.1);
     end
     
     meanThree = meanThree ./ size(tres,2);
     
     image(meanThree);
     % pause(0.5);
     
     
     meanFour = zeros(28);
     
     for i=1:size(quattro,2)
         new=reshape(xvalues(quattro(i),:), [28 28])';
         meanFour = meanFour + new;
         image(meanFour);
         % pause(0.1);
     end
     
     meanFour = meanFour ./ size(quattro,2);
     
     image(meanFour);
     % pause(0.5);
     
 meanFive = zeros(28);
     
     for i=1:size(cinco,2)
         new=reshape(xvalues(cinco(i),:), [28 28])';
         meanFive = meanFive + new;
         image(meanFive);
         % pause(0.1);
     end
     
     meanFive = meanFive ./ size(cinco,2);
     
     image(meanFive);
     % pause(0.5);
     
 meanSix = zeros(28);
     
     for i=1:size(seis,2)
         new=reshape(xvalues(seis(i),:), [28 28])';
         meanSix = meanSix + new;
         image(meanSix);
         % pause(0.1);
     end
     
     meanSix = meanSix ./ size(seis,2);
     
     image(meanSix);
     % pause(0.5);
     
meanSeven = zeros(28);
     
     for i=1:size(siete,2)
         new=reshape(xvalues(siete(i),:), [28 28])';
         meanSeven = meanSeven + new;
         image(meanSeven);
         % pause(0.1);
     end
     
     meanSeven = meanSeven ./ size(siete,2);
     
     image(meanSeven);
     % pause(0.5);
     
 meanEight = zeros(28);
     
     for i=1:size(ocho,2)
         new=reshape(xvalues(ocho(i),:), [28 28])';
         meanEight = meanEight + new;
         image(meanEight);
         % pause(0.1);
     end
     
     meanEight = meanEight ./ size(ocho,2);
     
     image(meanEight);
     % pause(0.5);
     
meanNine = zeros(28);
     
     for i=1:size(nueve,2)
         new=reshape(xvalues(nueve(i),:), [28 28])';
         meanNine = meanNine + new;
         image(meanNine);
         % pause(0.1);
     end
     
     meanNine = meanNine ./ size(nueve,2);
     
     image(meanNine);
     % pause(0.5);
     
       
meanZero = zeros(28);
     
     for i=1:size(cero,2)
         new=reshape(xvalues(cero(i),:), [28 28])';
         meanNine = meanNine + new;
         image(meanNine);
         % pause(0.1);
     end
     
     meanNine = meanNine ./ size(cero,2);
     
     image(meanNine);
     % pause(0.5);
     
     
     answer = meanOne;
     answer(:,:,2)=meanTwo;
     answer(:,:,3)=meanThree;
     answer(:,:,4)=meanFour;
     answer(:,:,5)=meanFive;
     answer(:,:,6)=meanSix;
     answer(:,:,7)=meanSeven;
     answer(:,:,8)=meanEight;
     answer(:,:,9)=meanNine;
     answer(:,:,10)=meanZero;
     
    
    result = struct('zero',meanZero,'one',meanOne,'two',meanTwo,'three',meanThree,'four',meanFour,'five',meanFive,'six',meanSix,'seven',meanSeven,'eight',meanEight,'nine',meanNine);
end

