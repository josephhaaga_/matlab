function [ temp  ] = knn_classify(  X,C,z,k  )
%KNN_Classify Classify query point by k-nearest neighbor majority vote
%   X is training data; C is a vector of corresponding training labels (of length n).
%   The values returned are ids, a vector of length k holding the indices (into X) of the k nearest
%   neighbors; and class, the majority vote over those neighbors; z = query
%   point; k= # neighbors

% train a classifier
d = compute_set_of_means(X,C);

g = [];
% find nearest neighbor in entire training set
g = knnsearch(X,z,'K',k);

ids = g;

% % for each nearest neighbor j...
 for j=1:k
     % find nearest neighbor in p, the set of all 'ideal' classes (mean
     % bitmaps)
     l = X(g(j),:);
     q= reshape(l, [28 28])';
     ids = knnsearch(d,q);
 end
% 
class = mean(ids);

temp = struct('ids',ids,'class',class);

end



