%Joseph Haaga 3.A

function[ perc ] = perceptron_build( data )
%PERCEPTRON_BUILD Implementation of perceptron
%   
 
    theta = [0; 0];					% Current parameters vector
    Xn = size(data.X, 1);			% create n by 1 vector
    
    mistakes = zeros(Xn, 1);        % mistakes vector
    histMistakes = [];
    mistakeCount=0;
    
    graph = figure;                         % graph window
    title('Perceptron');
    histogram = figure;                     % histogram
    title('Errors');
    
    % For each data point...
    for n = 1:Xn
        x = data.X(n,:)';				% get the coordinate pair...
        y = data.y(n);				% and the desired label.
        
        s = sign(y * (theta' * x));			% Assess if current classifier correctly labels it.
 
        if n == 1 || s < 0				% If labeled incorrecly...
            
            histMistakes(mistakeCount+1)=n;   % add datapoint number to histogram mistakes array
            mistakeCount=mistakeCount+1;        % increment mistakes counter
            p = struct('classifier',theta,'mistakes',mistakes)
            figure(graph);
            perceptron_display_mistakes(data,p,n);
            figure(histogram);
            hist(histMistakes,15);
            theta = theta + y*x;				%   update parameters vector..
            mistakes(n) = mistakes(n) + 1;		%   and increment mistakes vector.
        else
            %keep prior datapoints
            hold on;
            if y==1
                figure(graph);
                % graph 1 in green
                scatter(x(1), x(2), 10, 'g');
            else
                figure(graph);
                % graph -1 in black
                scatter(x(1), x(2), 10, 'k');
            end
            hold off;
        end
    end
    fprintf(' %s\n',histMistakes);
   
    
    perc = struct('classifier', theta, 'mistakes', mistakes,'histMistakes',histMistakes);
    
end
 
