%Joseph Haaga 3.B

function [ zero ] = perceptron_display_mistakes( data,perc,i )
%Perceptron_Display_Mistakes Displays mistakes of current preceptron
%classifier
%   Get the data points
    Xdata = data.X(i,:);
%   and the current classifier params
    theta = perc.classifier;
    
    len = sqrt(theta(1)^2+theta(2)^2);
    hold on;
    graphDomain = linspace(0,len);
    fTheta = theta(2)/theta(1)*Xdata;
    fCurrentClassifier = -theta(1)/theta(2)*Xdata;

    mistake = scatter(Xdata(1),Xdata(2),25,'r');
    pause(0.5);
    
%     hold off;
    
    zero=0;
    
    
    

end

